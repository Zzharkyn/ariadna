//paste this code under the head tag or in a separate js file.
	// Wait for window load
	jQuery(window).load(function() {
		// Animate loader off screen
		jQuery(".c-loader").fadeOut("slow");
	});

// MMENU

jQuery(document).ready(function() {
	jQuery("#my-menu").mmenu({
		counters: true,
	   "extensions": [
		  "pagedim-black",
		  "pageshadow",
		  "theme-dark"
	   ],
	   "offCanvas": {
		  "position": "right",
		  "zposition" : "front"
	   },
	   "navbars": [
		  {
			 "position": "top",
			 "content": [
				"<div class='search-container'></div>"
			 ]
		  },
		  {
			 "position": "top",
			 "content": [
			 	"<a class='moon-house-outline' href='/'></a>",
				"<a class='moon-remove' href='#/'></a>"
			 ]
		  },
		  {
			 "position": "bottom",
			 "content": [
				"<div class='social-nav'></div>"
			 ]
		  }
	   ]
	});
			var API = jQuery("#my-menu").data( "mmenu" );
	
			  jQuery(".moon-remove").click(function() {
				 API.close();
			  });
 });
 
 
// Slider

jQuery(document).ready(function ()
{
  jQuery('.o-main-slider').slick({
	  autoplay: true,
	  autoplaySpeed: 4000,
	  speed: 2000,
	  pauseOnHover: false,
	  arrows: true,
	  dots: false,
	  fade: true
  });

  jQuery("div.mm-search").appendTo("div.search-container");
  jQuery("div.menu-social").appendTo("div.social-nav");
  jQuery('div.menu-social').unwrap();
});

// News Ticker

jQuery(".modern-ticker").modernTicker({
	effect: "scroll",
	scrollType: "continuous",
	scrollStart: "inside",
	scrollInterval: 20,
	transitionTime: 500,
	linksEnabled: true,
	pauseOnHover: true,
	autoplay: true
});

/* Slideshow */

jQuery(document).ready(function() {
    jQuery('.pgwSlideshow').pgwSlideshow();
});

/* Twitter Widget */

window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);
 
  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };
 
  return t;
}(document, "script", "twitter-wjs"));

// Packery

// init Packery
		
var $grid = $('.pack-grid').packery({
	columnWidth: '.grid-sizer',
	// do not use .grid-sizer in layout
	itemSelector: '.grid-item',
	percentPosition: true
});

// layout Packery after each image loads
$grid.imagesLoaded().progress( function() {
	
  $grid.packery();
  
});

// Signature Page - Detect Scroll Position

$(document).ready(function(){
	
	$("nav.signature-nav ul li a").on("click", function( e ) {
    
		e.preventDefault();
	
		$("body, html").animate({ 
			scrollTop: $( $(this).attr('href') ).offset().top - 133
		}, 600);
		
	});

    /**
     * This part handles the highlighting functionality.
     * We use the scroll functionality again, some array creation and 
     * manipulation, class adding and class removing, and conditional testing
     */
	 
    var aChildren = $("nav.signature-nav ul li").children(); // find the a children of the list items
    var aArray = []; // create the empty aArray
    for (var i=0; i < aChildren.length; i++) {    
        var aChild = aChildren[i];
        var ahref = $(aChild).attr('href');
        aArray.push(ahref);
    } // this for loop fills the aArray with attribute href values
	

    $(window).scroll(function(){
        var windowPos = $(window).scrollTop() + 135; // get the offset of the window from the top of page
        var windowHeight = $(window).height(); // get the height of the window
        var docHeight = $(document).height();

        for (var i=0; i < aArray.length; i++) {
            var theID = aArray[i];
            var divPos = $(theID).offset().top; // get the offset of the div from the top of page
            var divHeight = $(theID).height(); // get the height of the div in question
            if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
                $("a[href='" + theID + "']").addClass("active");
            } else {
                $("a[href='" + theID + "']").removeClass("active");
            }
        }

        if(windowPos + windowHeight === docHeight) {
            if (!$("nav.signature-nav ul li:last-child a").hasClass("active")) {
                var navActiveCurrent = $(".active").attr("href");
                $("a[href='" + navActiveCurrent + "']").removeClass("active");
                $("nav.signature-nav ul li:last-child a").addClass("active");
            }
        }
    });
});


/* Sidebar Navigation */

jQuery(document).ready(function () {

	$(".nav-title").on("click", function() {
		$("#sidebar-nav").stop().slideToggle();
		$(this).toggleClass("active");
	});

	$("#sidebar-nav li").each(function () {
	    $("<span class='moon-arrow-down'></span>").insertBefore($(this).children("ul"));
	});

	$("#sidebar-nav span.moon-arrow-down").on("click", function() {
		$(this).next("ul").toggle();
		$(this).toggleClass("active");
	});
});


/* Share This */

jQuery(document).ready(function () {

    $(".share-this span.share-btn").on("click", function () {
        $(".share-this").toggleClass("active");
    });

});


/* Blog Nav */

jQuery(document).ready(function () {

 
	// Create the dropdown base
	$("<select />").appendTo(".o-blog-nav");
  
	// Create default option "Go to..."
	$("<option />", {
	  "selected": "selected",
	  "value"   : "",
	  "text"    : "Go to..."
	}).appendTo("div select");
  
	// Populate dropdown with menu items
	$(".o-blog-nav a").each(function() {
	  var el = $(this);
	  $("<option />", {
		"value"   : el.attr("href"),
		"text"    : el.text()
	  }).appendTo(".o-blog-nav select");
	});
  
	 // To make dropdown actually work
	 // To make more unobtrusive: https://css-tricks.com/4064-unobtrusive-page-changer/
	$(".o-blog-nav select").change(function() {
	  window.location = $(this).find("option:selected").val();
	});
  
});



// Recaptcha Responsive

var width = $('.g-recaptcha').parent().width();
if (width < 320) {
	var scale = width / 320;
	$('.g-recaptcha').css('transform', 'scale(' + scale + ')');
	$('.g-recaptcha').css('-webkit-transform', 'scale(' + scale + ')');
	$('.g-recaptcha').css('transform-origin', '0 0');
	$('.g-recaptcha').css('-webkit-transform-origin', '0 0');
}


/// Progress Bar
var ReadingPositionIndicator = window.ReadingPositionIndicator;
var rpi;
setTimeout(function waitUntilDomIsReadyLoadingCustomFontsMightOffsetThis() {
  rpi = new ReadingPositionIndicator({
    rpiArea: "[data-rpi-area]", // optional
    progressBar: {
      // optional
      show: true,
      color: "#d83935"
    }
    
  }).init();
}, 500);
/* Wait until DOM has fully rendered the rpiArea to get the calculations correct. 
If rpiArea is not used, then this should not be important. */

// rpi.destroy(); // use when to be removed
// rpi.update(); // optional force update, usage example: if you have updated the DOM and need to refresh the indicator


